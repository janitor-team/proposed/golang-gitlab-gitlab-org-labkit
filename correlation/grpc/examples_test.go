package grpccorrelation_test

import (
	"log"
	"net"

	grpccorrelation "gitlab.com/gitlab-org/labkit/correlation/grpc"
	"google.golang.org/grpc"
)

func Example_client() {
	// Add the interceptor to the grpc dialer
	dialer, err := grpc.Dial("https://gitaly-server.internal:9095",
		grpc.WithStreamInterceptor(grpccorrelation.StreamClientCorrelationInterceptor(
			grpccorrelation.WithClientName("my-client"),
		)),
		grpc.WithUnaryInterceptor(grpccorrelation.UnaryClientCorrelationInterceptor(
			grpccorrelation.WithClientName("my-client"),
		)),
	)

	if err != nil {
		log.Fatalf("unable to dial: %v", err)
	}

	// Use the client connection with a protobuf service here...

	defer dialer.Close()
}

func Example_server() {
	server := grpc.NewServer(
		grpc.StreamInterceptor(grpccorrelation.StreamServerCorrelationInterceptor()),
		grpc.UnaryInterceptor(grpccorrelation.UnaryServerCorrelationInterceptor()),
	)

	listener, err := net.Listen("unix", "/tmp/grpc")
	if err != nil {
		log.Fatalf("unable to listen: %v", err)
	}

	server.Serve(listener)
}
