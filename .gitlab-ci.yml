image: golang:1.16

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  REPO_NAME: gitlab.com/gitlab-org/labkit
  GO_SEMANTIC_RELEASE_VERSION: 2.12.0

  # Note, GOLANGCI_LINT_VERSION should match .tool-versions!
  GOLANGCI_LINT_VERSION: 1.41.1

stages:
  - build
  - verify
  - release

build_1.14:
  stage: build
  image: golang:1.14
  script:
    - ./compile.sh

build_1.15:
  stage: build
  image: golang:1.15
  script:
    - ./compile.sh

build_1.16:
  stage: build
  image: golang:1.16
  script:
    - ./compile.sh

test_1.14:
  stage: verify
  image: golang:1.14
  script:
    - ./test.sh

test_1.15:
  stage: verify
  image: golang:1.15
  script:
    - ./test.sh

test_1.16:
  stage: verify
  image: golang:1.16
  script:
    - ./test.sh

# The verify step should always use the same version of Go as devs are
# likely to be developing with to avoid issues with changes in these tools
# between go versions. Since these are simply linter warnings and not
# compiler issues, we only need a single version

sast:
  stage: verify

# Ensure that all the changes are backwards compatible with GitLab Workhorse
backwards_compat_gitaly:
  stage: verify
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.7-golang-1.15-git-2.29
  script:
    - ./backwords_compat.sh https://gitlab.com/gitlab-org/gitaly.git

# Ensure that all the changes are backwards compatible with GitLab Container Registry
backwards_compat_container_registry:
  stage: verify
  script:
    - ./backwords_compat.sh https://gitlab.com/gitlab-org/container-registry.git

mod tidy:
  stage: verify
  script:
    - ./tidy.sh

lint:
  image:
    name: golangci/golangci-lint:v${GOLANGCI_LINT_VERSION}-alpine
    entrypoint: ["/bin/sh", "-c"]
  # git must be installed for golangci-lint's --new-from-rev flag to work.
  before_script:
  - apk add --no-cache git bash jq
  stage: verify
  script:
    - ./lint.sh
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

commitlint:
  stage: verify
  image: node:14-alpine3.12
  before_script:
  - apk add --no-cache git
  - npm install
  script:
  - npx commitlint --from ${CI_MERGE_REQUEST_DIFF_BASE_SHA} --to HEAD --verbose
  rules:
  - if: $CI_MERGE_REQUEST_IID

release:
  image: node:14
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  script:
    - npm install
    - $(npm bin)/semantic-release
