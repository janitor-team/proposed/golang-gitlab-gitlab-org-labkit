package grpccorrelation

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_healthCheckFilterFunc(t *testing.T) {
	tests := []struct {
		name           string
		fullMethodName string
		want           bool
	}{
		{
			name:           "empty",
			fullMethodName: "",
			want:           true,
		},
		{
			name:           "normal",
			fullMethodName: "/gitaly.SSHService/SSHUploadPack",
			want:           true,
		},
		{
			name:           "normal",
			fullMethodName: grpcHealthCheckMethodFullName,
			want:           false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := healthCheckFilterFunc(context.Background(), tt.fullMethodName)
			require.Equal(t, tt.want, got)
		})
	}
}
